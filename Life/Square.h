#ifndef LIFE_SQUARE_H
#define LIFE_SQUARE_H


class Square {
    bool alive;
    bool readyToSpawn;
    bool readyToDie;
public:

    // Constructor
    Square();

    // Checks if there is a cell in the square -
    // returns true if there is one.
    bool isAlive();

    // Spawns a cell in this square,
    // sets "alive" to true.
    void spawnCell();

    // Kills the cell in this square,
    // sets "alive" to false.
    void killCell();

    // Sets "readyToDie" to "is" -
    // if "readyToDie" is set to true,
    // the cell in this square dies next turn.
    void setReadyToDie(bool is);

    // Sets "readyToSpawn" to "is" -
    // if "readyToSpawn" is set to true,
    // the cell in this square spawns next turn.
    void setReadyToSpawn(bool is);

    // Checks if the value of "readyToSpawn" is true.
    bool isReadyToSpawn();

    // Checks if the value of "readyToDie" is true.
    bool isReadyToDie();
};


#endif //LIFE_SQUARE_H

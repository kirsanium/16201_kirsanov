#include "Square.h"

Square::Square() {
    this->alive = false;
    this->readyToDie = false;
    this->readyToSpawn = false;
}

bool Square::isAlive() {
    return (this->alive);
}

void Square::spawnCell() {
    this->alive = true;
}

void Square::killCell() {
    if (!this->alive) return;
    this->alive = false;
}

bool Square::isReadyToSpawn() {
    return readyToSpawn;
}

bool Square::isReadyToDie() {
    return readyToDie;
}

void Square::setReadyToSpawn(bool is) {
    readyToSpawn = is;
}

void Square::setReadyToDie(bool is) {
    readyToDie = is;
}

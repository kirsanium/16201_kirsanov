#ifndef LIFE_GAMEMODEL_H
#define LIFE_GAMEMODEL_H
#include "Square.h"
#include <string>


// Game model class.
class GameModel {
    Square* field[10][10];
    int turnCounter;
    bool comeBack;
    bool changed;
    int checkNeighbors(int column, int line);
public:

    // Constructor
    GameModel();

    // Destructor
    ~GameModel();

    // Returns the current state of the field.
    bool* getField();

    // Returns the current value of the turn counter.
    int getTurnCounter();

    // Resets the field.
    void reset();

    // Sets a cell to the XY position, where X belongs to A-J (vertical lines)
    // and Y belongs to 0-9 (horizontal lines).
    void set(int column, int line);

    // Deletes a cell from the XY position, where X belongs to A-J (vertical lines)
    // and Y belongs to 0-9 (horizontal lines).
    void clear(int column, int line);

    // Makes num steps in the game.
    void step(int num = 1);

    // Makes 1 step back in the game.
    void back();

    // Saves a position to the file called "filename"
    // in the current directory.
    void save(std::string filename);

    // Loads a position from the file called "filename"
    // in the current directory.
    void load(std::string filename);

    // Checks if the state of the field was changed
    // as a result of an action.
    bool wasChanged();

    // Marks the field as unchanged.
    void setUnchanged();
};


#endif //LIFE_GAMEMODEL_H

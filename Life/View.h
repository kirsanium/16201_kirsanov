#ifndef LIFE_VIEW_H
#define LIFE_VIEW_H
#include "Square.h"
#include "GameModel.h"
#include <string>


// Controller + output class.
class View {
    GameModel *model;
    void fileSaved(std::string filename);
    void printField();
    void exit();
    void startGame();
public:

    // Constructor
    View(GameModel *model);

    // Starts the game loop.
    int run();
};


#endif //LIFE_VIEW_H

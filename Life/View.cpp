#include <iostream>
#include <regex>
#include "NumberOfStepsException.h"
#include "View.h"

View::View(GameModel *model) {
    this->model = model;
}

void View::printField() {
    bool *field = this->model->getField();
    for (int line = 9; line >= 0; --line) {
        std::cout << line << " ";
        for (int column = 0; column < 10; column++) {
            if (field[column*10 + line])
                std::cout << "*";
            else std::cout << ".";
        }
        std::cout << std::endl;
    }
    std::cout << "  ABCDEFGHIJ   " << "Turn: " << model->getTurnCounter() << std::endl;
}

int View::run() {
    std::string input;
    std::regex stepNum("step [[:digit:]]+");
    std::regex setXY("set [A-J][0-9]");
    std::regex clearXY("clear [A-J][0-9]");
    std::regex number("[[:digit:]]+");
    std::regex saveFilename("save \".+\"");
    std::regex loadFilename("load \".+\"");
    startGame();
    while (true) {
        try {
            std::getline(std::cin, input);
            if (input == "exit") {
                exit();
                return 0;
            } else if (input == "reset") {
                model->reset();
            } else if (input == "step") {
                model->step();
            } else if (input == "back") {
                model->back();
            } else if (std::regex_match(input, stepNum)) {
                std::sregex_iterator regexIterator = std::sregex_iterator(input.begin(), input.end(), number);
                std::smatch match = *regexIterator;
                std::string match_str = match.str();
                model->step(std::stoi(match_str));
            } else if (std::regex_match(input, setXY)) {
                int column;
                int line;
                switch (input[4]) {
                    case 'A':
                        column = 0;
                        break;
                    case 'B':
                        column = 1;
                        break;
                    case 'C':
                        column = 2;
                        break;
                    case 'D':
                        column = 3;
                        break;
                    case 'E':
                        column = 4;
                        break;
                    case 'F':
                        column = 5;
                        break;
                    case 'G':
                        column = 6;
                        break;
                    case 'H':
                        column = 7;
                        break;
                    case 'I':
                        column = 8;
                        break;
                    case 'J':
                        column = 9;
                        break;
                    default: {
                    }
                }
                line = input[5] - '0';
                model->set(column, line);
            } else if (std::regex_match(input, clearXY)) {
                int column;
                int line;
                switch (input[6]) {
                    case 'A':
                        column = 0;
                        break;
                    case 'B':
                        column = 1;
                        break;
                    case 'C':
                        column = 2;
                        break;
                    case 'D':
                        column = 3;
                        break;
                    case 'E':
                        column = 4;
                        break;
                    case 'F':
                        column = 5;
                        break;
                    case 'G':
                        column = 6;
                        break;
                    case 'H':
                        column = 7;
                        break;
                    case 'I':
                        column = 8;
                        break;
                    case 'J':
                        column = 9;
                        break;
                    default: {
                    }
                }
                line = input[7] - '0';
                model->clear(column, line);
            } else if (std::regex_match(input, loadFilename)) {
                std::string filename = input.substr(6, input.size() - 7);
                model->load(filename);
            } else if (std::regex_match(input, saveFilename)) {
                std::string filename = input.substr(6, input.size() - 7);
                model->save(filename);
                fileSaved(filename);
            } else {
                throw std::string("InvalidCommand");
            }
        }
        catch (std::string e) {
            if (e == "EndOfTheGame") {
                std::cout << "The game has ended at turn " << model->getTurnCounter() << std::endl;
            }
            else if (e == "CanNotComeBack") {
                std::cout << "Can not come back" << std::endl;
            }
            else if (e == "InvalidData") {
                std::string filename = input.substr(6, input.size() - 7);
                std::cout << "Invalid data in " << filename << std::endl;
            }
            else if (e == "InvalidTurnCounter") {
                std::string filename = input.substr(6, input.size() - 7);
                std::cout << "Invalid turn counter in " << filename << " ; turn counter set to 0" << std::endl;
            }
            else if (e == "InvalidCommand") {
                std::cout << "Invalid command" << std::endl;
            }
            else std::cout << "Unknown error" << std::endl;
        }
        catch (NumberOfStepsException &e) {
            std::cout << "Enter a number of steps between 1 and 1000" << std::endl;
        }
        if (model->wasChanged()) {
            printField();
            model->setUnchanged();
        }
    }
}

void View::fileSaved(std::string filename) {
    std::cout << "Current state was saved to " << filename << std::endl;
}

void View::startGame() {
    std::cout << "Welcome to the game of Life!" << std::endl;
}

void View::exit() {
    std::cout << "See you later!" << std::endl;
}


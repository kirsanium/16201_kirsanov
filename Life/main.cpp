#include "GameModel.h"
#include "View.h"

int main() {
    GameModel* model = new GameModel;
    View* view = new View(model);
    int toReturn = view->run();
    delete model;
    delete view;
    return toReturn;
}
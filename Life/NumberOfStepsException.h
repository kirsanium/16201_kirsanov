#ifndef LIFE_NUMBEROFSTEPSEXCEPTION_H
#define LIFE_NUMBEROFSTEPSEXCEPTION_H

#include <exception>
#include <string>

class NumberOfStepsException: std::exception
{
public:
    NumberOfStepsException(){};
};


#endif //LIFE_NUMBEROFSTEPSEXCEPTION_H

#include <iostream>
#include <fstream>
#include <regex>
#include "NumberOfStepsException.h"

#include "GameModel.h"

GameModel::GameModel() {
    for (int column = 0; column < 10; column++) {
        for (int line = 0; line < 10; line++) {
            field[column][line] = new Square();
        }
    }
    turnCounter = 0;
    comeBack = false;
    changed = false;
}

GameModel::~GameModel() {
    for (int column = 0; column < 10; column++) {
        for (int line = 0; line < 10; line++) {
            delete field[column][line];
        }
    }
}

void GameModel::reset() {
    for (int column = 0; column < 10; column++) {
        for (int line = 0; line < 10; line++) {
            field[column][line]->killCell();
        }
    }
    turnCounter = 0;
    changed = true;
}

void GameModel::set(int column, int line) {
    this->field[column][line]->spawnCell();
    changed = true;
}

void GameModel::clear(int column, int line) {
    this->field[column][line]->killCell();
    changed = true;
}

void GameModel::step(int num) {
    if (num < 1 || num > 1000) {
        throw NumberOfStepsException();
    }
    int neighboursAlive;
    for (int k = 0; k < num; k++) {
        changed = false;
        for (int column = 0; column < 10; column++) {
            for (int line = 0; line < 10; line++) {
                Square *square = field[column][line];
                square->setReadyToDie(false);
                square->setReadyToSpawn(false);
            }
        }
        for (int column = 0; column < 10; column++) {
            for (int line = 0; line < 10; line++) {
                Square *square = field[column][line];
                neighboursAlive = checkNeighbors(column, line);
                if (square->isAlive()) {
                    if (neighboursAlive < 2) {
                        square->setReadyToDie(true);
                        changed = true;
                    } else if (neighboursAlive > 3) {
                        square->setReadyToDie(true);
                        changed = true;
                    }
                } else if (!square->isAlive()) {
                    if (neighboursAlive == 3) {
                        square->setReadyToSpawn(true);
                        changed = true;
                    }
                }
            }
        }
        if (!changed) {
            changed = true;
            comeBack = false;
            throw std::string("EndOfTheGame");
        }
        ++turnCounter;
        for (int column = 0; column < 10; column++) {
            for (int line = 0; line < 10; line++) {
                Square *square = field[column][line];
                if (square->isReadyToDie()) {
                    square->killCell();
                } else if (square->isReadyToSpawn()) {
                    square->spawnCell();
                }
            }
        }
    }
    comeBack = true;
}

void GameModel::back() {
    try {
        if (!comeBack) {
            throw std::string("CanNotComeBack");
        }
        for (int column = 0; column < 10; column++) {
            for (int line = 0; line < 10; line++) {
                Square *square = field[column][line];
                if (square->isReadyToDie())
                    square->spawnCell();
                if (square->isReadyToSpawn())
                    square->killCell();
            }
        }
        --turnCounter;
        comeBack = false;
        changed = true;
    }
    catch (char const* e) {
        throw;
    }
}

void GameModel::save(std::string filename) {
    std::ofstream saveFile;
    saveFile.open(filename, std::ios_base::out | std::ios_base::trunc);
    for (int line = 9; line >= 0; --line) {
        for (int column = 0; column < 10; column++) {
            if (field[column][line]->isAlive())
                saveFile << "*";
            else saveFile << ".";
        }
        saveFile << std::endl;
    }
    saveFile << turnCounter;
}

void GameModel::load(std::string filename) {
    try {
        std::ifstream loadFile;
        std::string currentLine;
        std::string turn;
        std::regex number("[[:digit:]]+");
        loadFile.open(filename, std::ios_base::in);
        for (int line = 9; line >= 0; --line) {
            loadFile >> currentLine;
            for (int column = 0; column < 10; column++) {
                if (currentLine[column] == '.')
                    field[column][line]->killCell();
                else if (currentLine[column] == '*')
                    field[column][line]->spawnCell();
                else {
                    throw std::string("InvalidData");
                }
            }
        }
        loadFile >> turn;
        if (std::regex_match(turn, number)) {
            turnCounter = std::stoi(turn);
        }
        else {
            turnCounter = 0;
            changed = true;
            throw std::string("InvalidTurnCounter");
        }
        changed = true;
    }
    catch (char const* e) {
        throw;
    }
}

int GameModel::checkNeighbors(int column, int line) {
    int neighboursAlive = 0;
    int prevColumn = column - 1;
    if (prevColumn < 0) prevColumn += 10;
    int prevLine = line - 1;
    if (prevLine < 0) prevLine += 10;
    int nextColumn = column + 1;
    if (nextColumn > 9) nextColumn -= 10;
    int nextLine = line + 1;
    if (nextLine > 9) nextLine -= 10;
    if (field[prevColumn][prevLine]->isAlive()) ++neighboursAlive;
    if (field[prevColumn][line]->isAlive()) ++neighboursAlive;
    if (field[prevColumn][nextLine]->isAlive()) ++neighboursAlive;
    if (field[column][prevLine]->isAlive()) ++neighboursAlive;
    if (field[column][nextLine]->isAlive()) ++neighboursAlive;
    if (field[nextColumn][prevLine]->isAlive()) ++neighboursAlive;
    if (field[nextColumn][line]->isAlive()) ++neighboursAlive;
    if (field[nextColumn][nextLine]->isAlive()) ++neighboursAlive;
    return neighboursAlive;
}

bool* GameModel::getField() {
    bool *field = new bool[100];
    for (int column = 0; column < 10; column++) {
        for (int line = 0; line < 10; line++) {
            field[column*10 + line] = this->field[column][line]->isAlive();
        }
    }
    return field;
}

int GameModel::getTurnCounter() {
    return this->turnCounter;
}

bool GameModel::wasChanged() {
    return this->changed;
}

void GameModel::setUnchanged() {
    this->changed = false;
}



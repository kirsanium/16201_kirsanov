#include <iostream>
#include "LinkedList.h"


const value_type &LinkedList::const_iterator::operator*() const {
    return this->pointer->value;
}


const value_type *LinkedList::const_iterator::operator->() const {
    return &this->pointer->value;
}


LinkedList::base_iterator &LinkedList::base_iterator::operator=(const LinkedList::base_iterator &other) {
    this->pointer = other.pointer;
}

bool LinkedList::base_iterator::operator!=(const LinkedList::base_iterator &other) const {
    return this->pointer != other.pointer;
}

bool LinkedList::base_iterator::operator==(const LinkedList::base_iterator &other) const {
    return this->pointer == other.pointer;
}


value_type &LinkedList::iterator::operator*() {
    return this->pointer->value;
}


value_type *LinkedList::iterator::operator->() {
    return &this->pointer->value;
}

LinkedList::iterator::iterator(LinkedList::base_iterator iterator) {
    this->pointer = iterator.pointer;
}

LinkedList::const_iterator::const_iterator(LinkedList::base_iterator iterator) {
    this->pointer = iterator.pointer;
}

LinkedList::base_iterator &LinkedList::base_iterator::operator++() {
	this->pointer = this->pointer->next;
	return *this;
}

LinkedList::base_iterator LinkedList::base_iterator::operator++(int) {
    LinkedList::base_iterator temp;
    temp.pointer = this->pointer;
    this->pointer = this->pointer->next;
    return temp;
}

LinkedList::base_iterator &LinkedList::base_iterator::operator--() {
	this->pointer = this->pointer->prev;
	return *this;
}

LinkedList::base_iterator LinkedList::base_iterator::operator--(int) {
	base_iterator temp;
	temp.pointer = this->pointer;
	this->pointer = this->pointer->prev;
	return temp;
}

LinkedList::LinkedList() {
    Node *node = new Node;
    node->next = node->prev = node;
    this->endNode = node;
    this->elemAmount = 0;
}

LinkedList::LinkedList(const LinkedList &other) {
    Node *node = new Node;
    node->next = node->prev = node;
    this->endNode = node;
    this->elemAmount = 0;
    Node* current = other.endNode->next;
    while (current != other.endNode) {
        this->push_back(current->value);
        current = current->next;
    }
}

LinkedList::LinkedList(LinkedList &&other) {
    this->endNode = other.endNode;
    this->elemAmount = other.elemAmount;
    Node *node = new Node;
    node->next = node->prev = node;
    other.endNode = node; //other.endNode->next = other.endNode = node; ???
    other.elemAmount = 0;
}

LinkedList::~LinkedList() {
    this->clear();
	delete this->endNode;
}

LinkedList &LinkedList::operator=(const LinkedList &other) {
    LinkedList *temp = new LinkedList(other);
    return *temp;
}

LinkedList &LinkedList::operator=(LinkedList &&other) {
    LinkedList *temp = new LinkedList(other);
    return *temp;
}

LinkedList::iterator LinkedList::begin() {
    LinkedList::iterator it;
    it.pointer = this->endNode->next;
    return it;
}
LinkedList::const_iterator LinkedList::begin() const {
    LinkedList::const_iterator it;
    it.pointer = this->endNode->next;
    return it;
}

LinkedList::const_iterator LinkedList::cbegin() const {
    LinkedList::const_iterator it;
    it.pointer = this->endNode->next;
    return it;
}

LinkedList::base_iterator LinkedList::end() {
    LinkedList::base_iterator it;
    it.pointer = this->endNode;
    return it;
}

int LinkedList::size() const {
    return this->elemAmount;
}

bool LinkedList::empty() const {
    return this->elemAmount == 0;
}


value_type &LinkedList::front() {
    return this->endNode->next->value;
}


const value_type &LinkedList::front() const {
    return this->endNode->next->value;
}


value_type &LinkedList::back() {
    return this->endNode->prev->value;
}


const value_type &LinkedList::back() const {
	return this->endNode->prev->value;
}

LinkedList::base_iterator LinkedList::erase(LinkedList::base_iterator position){
    if (position == this->end()) {
        std::cerr << "An exception occurred";
        return position;
    }
    LinkedList::base_iterator temp;
    position++;
    temp = position;
    position--;
    position.pointer->prev->next = position.pointer->next;
    position.pointer->next->prev = position.pointer->prev;
    delete position.pointer;
    this->elemAmount--;
    return temp;
}

LinkedList::base_iterator LinkedList::erase(LinkedList::base_iterator begin, LinkedList::base_iterator end) {
    if (begin == end)
        return begin;
    for (LinkedList::base_iterator temp = begin; temp != end; temp++) {
        if (temp.pointer == this->endNode) {
            std::cerr << "Iterators point at different lists or begin > end";
            return begin;
        }
    }
    begin.pointer->prev->next = end.pointer;
    end.pointer->prev = begin.pointer->prev;
    for (LinkedList::base_iterator temp = begin; begin != end; temp = begin) {
        begin++;
        delete temp.pointer;
        this->elemAmount--;
    }
    return begin;
}



int LinkedList::remove(const value_type &value) {
    int nodesRemoved = 0;
    LinkedList::base_iterator it = this->begin();
    LinkedList::base_iterator temp;
    while (it.pointer != this->endNode) {
        temp = it;
        it++;
        if (temp.pointer->value == value) {
            erase(temp);
            nodesRemoved++;
        }
    }
    return nodesRemoved;
}

void LinkedList::clear() {
    LinkedList::base_iterator begin = this->begin();
    LinkedList::base_iterator end = this->end();
    erase(begin, end);
}

void LinkedList::pop_back() {
    LinkedList::base_iterator it = this->end();
    it--;
    erase(it);
}

void LinkedList::pop_front() {
    LinkedList::base_iterator it = this->begin();
    erase(it);
}


void LinkedList::push_back(const value_type &value) {
	insert(this->end(), value);
}


void LinkedList::push_front(const value_type &value) {
	insert(this->begin(), value);
}


LinkedList::base_iterator LinkedList::insert(LinkedList::base_iterator before, const value_type &value) {
    Node *node = new Node;
    node->next = before.pointer;
    node->prev = before.pointer->prev;
    before.pointer->prev = node;
	node->prev->next = node;
    before--;
    node->value = value;
    this->elemAmount++;
    return before;
}

LinkedList &LinkedList::operator+=(const LinkedList &other) {
    if (other.empty()) return *this;
    for (Node* node = other.endNode->next; node != other.endNode; node = node->next) {
        this->push_back(node->value);
    }
    return *this;
}

/* Операторы внешние */
//Сравнивает 2 листа

bool operator!=(const LinkedList & left, const LinkedList & right) {
    Node* node1 = left.endNode->next;
    Node* node2 = right.endNode->next;
    while (node1 != left.endNode || node2 != right.endNode) {
        if (node1->value != node2->value)
            return true;
        node1 = node1->next;
        node2 = node2->next;
    }
    return (node1 != left.endNode || node2 != right.endNode);
}


bool operator==(const LinkedList & left, const LinkedList & right) {
    Node* node1 = left.endNode->next;
    Node* node2 = right.endNode->next;
    while (node1 != left.endNode || node2 != right.endNode) {
        if (node1->value != node2->value)
            return false;
        node1 = node1->next;
        node2 = node2->next;
    }
    return !(node1 != left.endNode || node2 != right.endNode);
}



//Возвращает лист объединяющий 2 листа.

LinkedList operator+(const LinkedList & left, const LinkedList & right) {
    if (right.empty()) return left;
    if (left.empty()) return right;
    LinkedList temp = left;
    for (Node* node = right.endNode->next; node != right.endNode; node = node->next) {
        temp.push_back(node->value);
    }
    return temp;
}

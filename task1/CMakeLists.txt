cmake_minimum_required(VERSION 3.7)
project(task1)

set(CMAKE_CXX_STANDARD 11)
add_subdirectory("C:/Programs/googletest/googletest" "${CMAKE_CURRENT_BINARY_DIR}/testlib_build")
include_directories(${gtest_SOURCE_DIR}/include ${gtest_SOURCE_DIR})
set(SOURCE_FILES LinkedList.cpp LinkedList.h main.cpp)
add_executable(task1 ${SOURCE_FILES})

target_link_libraries(task1 gtest gtest_main)

#ifndef TASK1_LINKEDLIST_H
#define TASK1_LINKEDLIST_H

typedef int value_type;

typedef struct Node_t {
    struct Node_t * prev;
    struct Node_t * next;
    value_type value;
} Node;


class LinkedList {
    Node* endNode;
    int elemAmount;
public:
    friend bool operator!=(const LinkedList & left, const LinkedList & right);
    friend bool operator==(const LinkedList & left, const LinkedList & right);
    friend LinkedList operator+(const LinkedList & left, const LinkedList & right);

    //Iterators allow you to conveniently look through the elements
    //of the list.
    class base_iterator {
        Node *pointer;
        base_iterator(){};
    public:
        friend class LinkedList;

        //Makes iterator equal to the "other".
        base_iterator & operator=(const base_iterator & other);
        //Compares iterators - return true if they point
        //at different elements (not different values).
        bool operator!=(const base_iterator & other) const;
        //Compares iterators - return true if they point
        //at the same elements (not the same values).
        bool operator==(const base_iterator & other) const;

        base_iterator & operator++();
        base_iterator operator++(int);
        base_iterator & operator--();
        base_iterator operator--(int);
    };

    //This iterator allows you to get constant
    //values of elements.
    class const_iterator: public base_iterator {
    public:
        //Constructors
        const_iterator(){this->pointer = nullptr;};
        const_iterator(base_iterator iterator);
        //Returns the reference to the current element of the list.
        const value_type & operator*() const;
        //Returns the pointer to the current element of the list
        const value_type * operator->() const;
    };

    //This iterator allows you to get non-constant
    //values of elements.
    class iterator: public base_iterator {
    public:
        //Constructors
        iterator(){this->pointer = nullptr;};
        iterator(base_iterator iterator);
        //Returns the reference to the current element of the list.
        value_type & operator*(); //done
        //Returns the pointer to the current element of the list
        value_type * operator->();
    };
    friend base_iterator;

    //Constructors
    LinkedList();
    LinkedList(const LinkedList & other);
    LinkedList(LinkedList && other);

    //Destructor
    ~LinkedList();

    //Makes the list equal to the "other"
    LinkedList & operator=(const LinkedList & other);
    LinkedList & operator=(LinkedList && other);

    //Returns iterator, pointing at the first element in the list.
    iterator begin();
    const_iterator begin() const;

    const_iterator cbegin() const;
    //Returns iterator, pointing at the element of the list,
    //which follows the last one, i.e. at the one not existing yet.
    base_iterator end();

    //Returns the size of the list.
    int size() const;
    //Returns true if the list is empty.
    bool empty() const;

    //Returns the reference to the first element in the list.
    value_type & front();
    const value_type & front() const;
    //Returns the reference to the last element in the list.
    value_type & back();
    const value_type & back() const;

    //Deletes the element which "position" iterator points at.
    base_iterator erase(base_iterator position);
    //Deletes elements in [begin, end) interval.
    base_iterator erase(base_iterator begin, base_iterator end);
    //Deletes all "value" entries it finds.
    //Returns amount of deleted elements.
    int remove(const value_type & value);
    //Deletes all elements from the list.
    void clear(); //done

    //Deletes the last element of the list.
    void pop_back();
    //Deletes the first element of the list.
    void pop_front();
    //Adds value to the end of the list.
    void push_back(const value_type & value);
    //Adds value to the beginning of the list.
    void push_front(const value_type & value);
    //Inserts value before the element which "before" iterator points at.
    base_iterator insert(base_iterator before, const value_type & value);
    //Adds "other" list to the end of the list.
    LinkedList & operator+=(const LinkedList & other);
};


/* Compares two lists. Returns true if they are unequal
 * and false if they are equal. */
bool operator!=(const LinkedList & left, const LinkedList & right);

/* Compares two lists. Returns false if they are unequal
 * and true if they are equal. */
bool operator==(const LinkedList & left, const LinkedList & right);

/* Adds "right" list to the end of the "left" list. */
LinkedList operator+(const LinkedList & left, const LinkedList & right);

#endif


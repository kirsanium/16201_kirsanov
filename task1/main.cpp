#include "LinkedList.h"
#include "gtest/gtest.h"
namespace {

    TEST(ConstructorsTest, Constructor) {
        LinkedList linkedList;
        LinkedList::iterator it = linkedList.end();
        LinkedList::iterator it1 = it;
        it1++;
        ASSERT_EQ(it, it1);
        LinkedList::base_iterator it2 = it;
        it2--;
        ASSERT_EQ(it, it2);
        ASSERT_EQ(it1, it2);
    }

    TEST(ConstructorsTest, CopyConstructor) {
        LinkedList list1;
        list1.push_back(1);
        list1.push_back(2);
        list1.push_back(3);
        LinkedList list2(list1);
        ASSERT_TRUE(list1 == list2);  //ASSERT_EQ(list1, list2) doesn't work for some reason
        list1.push_back(4);
        ASSERT_TRUE(list1 != list2);
    }

    TEST(ConstructorsTest, MoveConstructor) {
        LinkedList list1;
        list1.push_back(1);
        list1.push_back(2);
        list1.push_back(3);
        LinkedList list2 = std::move(list1);
        LinkedList::iterator it2 = list2.begin();
        ASSERT_EQ(1, *it2);
        it2++;
        ASSERT_EQ(2, *it2);
    }

    TEST(AddingTest, PushBack) {
        LinkedList list1;
        list1.push_back(1);
        list1.push_back(2);
        LinkedList::iterator it1 = list1.begin();
        ASSERT_EQ(1, *it1);
    }

    TEST(AddingTest, PushFront) {
        LinkedList list1;
        list1.push_front(1);
        list1.push_front(2);
        LinkedList::iterator it1 = list1.begin();
        ASSERT_EQ(2, *it1);
    }

    TEST(AddingTest, Insert) {
        LinkedList list1;
        list1.push_back(1);
        list1.push_back(2);
        list1.push_back(3);
        LinkedList::iterator it = list1.begin();
        it = list1.insert(it, 5);
        ASSERT_EQ(5, *it);
        it = list1.begin();
        ASSERT_EQ(5, *it);
    }

    TEST(AddingTest, AddingLists) {
        LinkedList list1;
        list1.push_back(1);
        list1.push_back(2);
        list1.push_back(3);
        LinkedList list2;
        list2.push_back(4);
        list2.push_back(5);
        list2.push_back(6);
        LinkedList list3 = list1 + list2;
        list1.push_back(4);
        list1.push_back(5);
        list1.push_back(6);
        ASSERT_TRUE(list1 == list3);
        list1.pop_back();
        list1.pop_back();
        list1.pop_back();
        list1 += list2;
        ASSERT_TRUE(list1 == list3);
    }

    TEST(ElemAmount, Empty) {
        LinkedList list;
        ASSERT_TRUE(list.empty());
        list.push_back(1);
        list.pop_back();
        ASSERT_TRUE(list.empty());
        list.push_front(1);
        list.pop_front();
        ASSERT_TRUE(list.empty());
        list.push_back(1);
        LinkedList::iterator it = list.begin();
        list.erase(it);
        ASSERT_TRUE(list.empty());
    }

    TEST(ElemAmount, Size) {
        LinkedList list;
        list.push_front(1);
        ASSERT_EQ(1, list.size());
        list.push_back(2);
        ASSERT_EQ(2, list.size());
        LinkedList::iterator it = list.begin();
        list.insert(it, 3);
        ASSERT_EQ(3, list.size());
        it = list.begin();
        LinkedList::iterator it2 = it;
        it2++;
        list.erase(it, it2);
        ASSERT_EQ(2, list.size());
        list.clear();
        ASSERT_EQ(0, list.size());
        list.push_front(1);
        list.push_front(1);
        list.push_front(2);
        ASSERT_EQ(3, list.size());
        list.remove(1);
        ASSERT_EQ(1, list.size());
        it = list.begin();
        list.erase(it);
        ASSERT_EQ(0, list.size());
    }

    TEST(References, FrontReferences) {
        LinkedList list1;
        list1.push_back(1);
        list1.push_back(2);
        list1.push_back(3);
        int &reference = list1.front();
        ASSERT_EQ(1, reference);
        reference = 5;
        LinkedList::iterator it = list1.begin();
        ASSERT_EQ(*it, reference);
        const int &const_reference = list1.front();
        reference = 7;
        ASSERT_EQ(7, const_reference);
    }

    TEST(References, BackReferences) {
        LinkedList list;
        list.push_front(1);
        list.push_front(2);
        list.push_front(3);
        int &reference = list.back();
        ASSERT_EQ(1, reference);
        reference = 5;
        LinkedList::iterator it = list.end();
        --it;
        ASSERT_EQ(*it, reference);
        ASSERT_EQ(*it, 5);
        const int &const_reference = list.back();
        ASSERT_EQ(5, const_reference);
        reference = 7;
        ASSERT_EQ(7, const_reference);
    }
    TEST(IteratorOperators, ArrowOperator) {
        LinkedList list;
        list.push_back(1);
        list.push_back(2);
        list.push_back(3);
        LinkedList::iterator it = list.begin();
        int *x = it.operator->();
        ASSERT_EQ(1, *x);
        LinkedList::const_iterator constit = list.begin();
        const int *y = constit.operator->();
        ASSERT_EQ(1, *y);
    }

    TEST(IteratorOperators, Other) {
        LinkedList list;
        list.push_back(1);
        list.push_back(2);
        list.push_back(3);
        LinkedList::iterator it = list.begin();
        LinkedList::iterator temp = it++;
        ASSERT_TRUE(temp != it);
        ASSERT_EQ(1, *temp);
        ASSERT_EQ(2, *it);
        temp = ++it;
        ASSERT_EQ(3, *temp);
        ASSERT_EQ(3, *it);
        temp = it--;
        ASSERT_EQ(3, *temp);
        ASSERT_EQ(2, *it);
        temp = --it;
        ASSERT_EQ(1, *temp);
        ASSERT_EQ(1, *it);
        LinkedList::const_iterator constit = list.begin();
        ASSERT_EQ(1, *constit);
    }
}

int main(int argc, char **argv) {
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}
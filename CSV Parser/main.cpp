#include <iostream>
#include <fstream>
#include <tuple>
#include "CSVParser.h"

using namespace TupleUtils;

int main()
{
    std::ifstream file("test.csv");
    CSVParser<int, std::string> parser(file, 0, 'z', '.', '\'' /*skip first lines count*/);
    try {
        for (std::tuple<int, std::string> rs : parser) {
            std::cout << rs << std::endl;
        }
    }
    catch (CSVReadError e) {
        std::cout << e.what() << std::endl;
    }
}

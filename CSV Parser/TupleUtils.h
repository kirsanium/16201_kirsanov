#ifndef CSV_PARSER_TUPLEUTILS_H
#define CSV_PARSER_TUPLEUTILS_H

#include <fstream>
#include <tuple>
#include <iostream>
#include <vector>
#include <sstream>


namespace TupleUtils{
    namespace {
        template <typename type, unsigned N, unsigned last>
        struct TuplePrinter {
            static void print(std::ostream& os, const type& tuple) {
                os << std::get<N>(tuple) << " ";
                TuplePrinter<type, N+1, last>::print(os, tuple);
            }
        };
        template <typename type, unsigned N>
        struct TuplePrinter <type, N, N>{
            static void print(std::ostream& os, const type& tuple) {
                os << std::get<N>(tuple);
            }
        };
    }
    template<typename... Types>
    std::ostream& operator<<(std::ostream& os, std::tuple<Types...>& tuple) {
        os << "(";
        TuplePrinter<std::tuple<Types...>, 0, sizeof...(Types) - 1>::print(os, tuple);
        os << ")";
        return os;
    }
    template<>
    std::ostream& operator<<(std::ostream& os, std::tuple<>& tuple) {
        os << "(" << ")";
        return os;
    }

    //  DEFAULT INIT  //
    namespace  {
        template <typename type, unsigned N, unsigned last>
        struct TupleDefaultInitiator {
            using NthType = typename std::tuple_element<N, type>::type;
            static void init(type& tuple, std::size_t start) {
                if (N >= start)
                    std::get<N>(tuple) =  NthType();
                TupleDefaultInitiator<type, N + 1, last>::init(tuple, start);
            }
        };

        template <typename type, unsigned N>
        struct TupleDefaultInitiator <type, N, N> {
            using NthType = typename std::tuple_element<N, type>::type;
            static void init(type& tuple, std::size_t start) {
                if (N >= start)
                    std::get<N>(tuple) = NthType();
            }
        };
    }
    template<typename... Types>
    void DefaultInitTuple(std::tuple<Types...>& tpl, std::size_t start = 0) {
        TupleDefaultInitiator<std::tuple<Types...>, 0, sizeof...(Types) - 1>::init(tpl, start);
    }


    //  ASSIGN TUPLE TO VECTOR  //
    namespace {
        template<typename Type>
        struct TupleAssignElemntOfVectorHelper {
            static void assign(Type& elem, const std::string& str) {
                std::stringstream sstr(str);
                sstr >> elem;
            }
        };
        template<>
        struct TupleAssignElemntOfVectorHelper<std::string> {
            static void assign(std::string& elem, const std::string& str) {
                elem = str;
            }
        };
        template<typename Type, unsigned N, unsigned Last>
        struct TupleAssignVectorHelper {
            using NthType = typename std::tuple_element<N, Type>::type;
            static void assign(Type& tuple, const std::vector<std::string>& vec) {
                TupleAssignElemntOfVectorHelper<NthType>::assign(std::get<N>(tuple), vec[N]);
                return TupleAssignVectorHelper<Type, N + 1, Last>::assign(tuple, vec);
            }
        };
        template<typename Type, unsigned N>
        struct TupleAssignVectorHelper<Type, N, N> {
            using NthType = typename std::tuple_element<N, Type>::type;
            static void assign(Type& tuple, const std::vector<std::string>& vec) {
                TupleAssignElemntOfVectorHelper<NthType>::assign(std::get<N>(tuple), vec[N]);
            }
        };
    }
    template<typename... Types>
    void assignTupleToVector(std::tuple<Types...>& tuple, const std::vector<std::string>& vec) {
        DefaultInitTuple(tuple);
        TupleAssignVectorHelper<std::tuple<Types...>, 0, sizeof...(Types) - 1>::assign(tuple, vec);
    }
}

#endif //CSV_PARSER_TUPLEUTILS_H

#ifndef CSV_PARSER_CSVPARSER_H
#define CSV_PARSER_CSVPARSER_H

#include <tuple>
#include <fstream>
#include <iostream>
#include <vector>
#include "TupleUtils.h"

class CSVReadError: std::exception {
private:
    std::string error;
public:
    CSVReadError(size_t row, size_t col) {
        error = "Reading failed on row #" + std::to_string(row) + ", column #" + std::to_string(col);
    }
    CSVReadError(std::string error) {
        this->error = error;
    }

    virtual const char* what() const noexcept override{
        return error.c_str();
    }
};

template <typename ...Types>
class CSVParser {
private:
    std::istream &is;
    size_t currentRow;
    size_t currentCol;
    char rowDelim;
    char colonDelim;
    char escChar;

public:
    class iterator;
    CSVParser(std::istream &is = std::cin, int rowsToSkip = 0, char rowDelim = '\n', char colonDelim = ',', char escChar = '"'):
            is(is), currentRow(0), rowDelim(rowDelim), colonDelim(colonDelim), escChar(escChar), currentCol(0)
    {
        int count = 0;
        while(count < rowsToSkip && is.good()) {
            char c;
            is.get(c);
            if (c == rowDelim) {
                ++count;
                ++this->currentRow;
            }
        }
    }
    iterator begin() {
        return iterator(*this);
    }
    iterator end() {
        return iterator();
    }
    class iterator : std::iterator<std::input_iterator_tag, std::tuple<Types...>> {
    private:
        std::tuple<Types...> cur;
        CSVParser* parser;
        void parseRow() {
            if (!parser->is.good()) {
                TupleUtils::DefaultInitTuple(cur);
                return;
            }
            ++parser->currentRow;
            std::vector<std::string> vec(sizeof...(Types), "");
            bool screened = false;
            char c;
            for (size_t i = 0; i < sizeof...(Types) - 1; ++i) {
                while (true) {
                    if (!parser->is.good()) {
                        TupleUtils::DefaultInitTuple(cur);
                        throw CSVReadError(parser->currentRow, parser->currentCol);
                    }
                    parser->is.get(c);
                    if (c == parser->escChar) {
                        screened = !screened;
                        continue;
                    }
                    if (!screened) {
                        if (c == parser -> colonDelim) {
                            ++parser -> currentCol;
                            break;
                        }
                        else if (c == parser ->rowDelim) {
                            throw CSVReadError(parser->currentRow, parser->currentCol);
                        }
                        else {
                            vec[i].push_back(c);
                        }
                    }
                    else {
                        vec[i].push_back(c);
                    }
                }
            }
            while (true) {
                parser->is.get(c);
                if (!parser->is.good()) {
                    break;
                }
                if (c == parser->escChar) {
                    screened = !screened;
                    continue;
                }
                if (!screened) {
                    if (c == parser -> colonDelim) {
                        throw CSVReadError(parser->currentRow, parser->currentCol + 1);
                    }
                    else if (c == parser ->rowDelim) {
                        break;
                    }
                    else {
                        vec[sizeof...(Types) - 1].push_back(c);
                    }
                }
                else {
                    vec[sizeof...(Types) - 1].push_back(c);
                }
            }
            TupleUtils::assignTupleToVector(cur, vec);
        }
    public:
        iterator() {
            TupleUtils::DefaultInitTuple(cur);
        }
        iterator(CSVParser& parser): parser(&parser) {
            parseRow();
        }
        iterator(const iterator& oth) : cur(oth.cur), parser(oth.parser) {}
        iterator& operator=(const iterator& oth) {
            cur = oth.cur;
            parser = oth.parser;
        }
        ~iterator() {}
        bool operator==(const iterator& oth) const {
            return cur == oth.cur;
        }
        bool operator!=(const iterator& oth) const {
            return !operator==(oth);
        }
        std::tuple<Types...>& operator*() {
            return cur;
        }
        const std::tuple<Types...>& operator*() const{
            return cur;
        }
        std::tuple<Types...>* operator->() {
            return &cur;
        }
        const std::tuple<Types...>* operator->() const{}
        iterator & operator++() {
            parseRow();
            return *this;
        }
        iterator operator++(int) {
            iterator buf = *this;
            ++*this;
            return buf;
        }
        friend void swap(iterator& a, iterator& b) {
            std::swap(a.cur,b.cur);
            std::swap(a.parser, b.parser);
        }
    };
};

#endif //CSV_PARSER_CSVPARSER_H

#ifndef WORKFLOW_BLOCKFACTORY_H
#define WORKFLOW_BLOCKFACTORY_H

#include <map>
#include <vector>
#include "WorkflowBlock.h"
#include "WorkflowExceptions.h"

class AbstractWorkflowBlockCreator {
public:
    AbstractWorkflowBlockCreator(){};
    virtual ~AbstractWorkflowBlockCreator(){};
    virtual WorkflowBlock* create() const = 0;
};

template<typename C>
class WorkflowBlockCreator: public AbstractWorkflowBlockCreator {
public:
    WorkflowBlockCreator(){};
    virtual ~WorkflowBlockCreator(){};
    WorkflowBlock* create() const { return new C(); };
};

class WorkflowBlockFactory {
private:
    WorkflowBlockFactory(){};

    typedef std::map<std::string, AbstractWorkflowBlockCreator*> FactoryMap;
    FactoryMap factoryMap;
public:
    WorkflowBlockFactory(const WorkflowBlockFactory &) = delete;
    WorkflowBlockFactory& operator= (const WorkflowBlockFactory&) = delete;
    static WorkflowBlockFactory& Instance()
    {
        static WorkflowBlockFactory instance;
        return instance;
    }

    virtual ~WorkflowBlockFactory(){};

    template <typename C>
    void add(const std::string & id)
    {
        FactoryMap::iterator it = factoryMap.find(id);
        if (it == factoryMap.end()) {
            factoryMap[id] = new WorkflowBlockCreator<C>();
        }
    }

    WorkflowBlock* create(std::string & id)
    {
        if (factoryMap.find(id) != factoryMap.end())
            return factoryMap[id]->create();
        else throw new WrongInstructionsFormatException();
    }

    std::vector<std::string> getAllBlockNames()
    {
        std::vector<std::string> names;
        for (FactoryMap::iterator it = factoryMap.begin(); it != factoryMap.end(); it++)
            names.push_back(it->first);
        return names;
    }


};

#endif //WORKFLOW_BLOCKFACTORY_H

#include <iostream>
#include "WorkflowBlockFactory.h"
#include "InstructionsParser.h"

int main(int argc, char** argv) {
    try {
        if (argc != 2) throw WrongInputException();
        InstructionsParser *parser = new InstructionsParser();
        parser->parseInstructions(argv[1]);
        delete parser;
    }
    catch (WrongInputException &e)
    {
        std::cout << "Enter the filename" << std::endl;
    }
}
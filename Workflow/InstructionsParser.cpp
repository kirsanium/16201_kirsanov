//
// Created by Никита on 02.11.2017.
//
#include <iostream>
#include <fstream>
#include <regex>
#include "WorkflowBlockFactory.h"
#include "WorkflowBlockInherited.h"
#include "InstructionsParser.h"

inline bool space(char c){
    return std::isspace(c);
}

inline bool notspace(char c){
    return !std::isspace(c);
}

//break a sentence into words
std::vector<std::string> split(const std::string& s){
    typedef std::string::const_iterator iter;
    std::vector<std::string> ret;
    iter i = s.begin();
    while(i!=s.end()){
        i = std::find_if(i, s.end(), notspace); // find the beginning of a word
        iter j= std::find_if(i, s.end(), space); // find the end of the same word
        if(i!=s.end()){
            ret.push_back(std::string(i, j)); //insert the word into vector
            i = j; // repeat 1,2,3 on the rest of the line.
        }
    }
    return ret;
}

std::map<std::string, WorkflowBlock *> parseDescription(std::ifstream& ifstream) {
    std::map<std::string, WorkflowBlock *> blockMap;
    std::string buffer;
    std::regex number("[[:digit:]]+");
    std::getline(ifstream, buffer);
    if (buffer != "desc")
        throw new WrongInstructionsFormatException();
    std::getline(ifstream, buffer);
    while (buffer != "csed") {
        if (!ifstream)
            throw WrongInstructionsFormatException();
        std::vector<std::string> words = split(buffer);
        if (!std::regex_match(words[0], number) || blockMap.find(words[0]) != blockMap.end())
            throw WrongInstructionsFormatException();
        if (words[1] != "=")
            throw WrongInstructionsFormatException();
        WorkflowBlock *block = WorkflowBlockFactory::Instance().create(words[2]);
        blockMap[words[0]] = block;
        block->set_id(words[0]);
        std::vector<std::string> params;
        if (words.size() != block->getParamAmount() + 3)
            throw WrongInstructionsFormatException();
        for (int i = 0; i < block->getParamAmount(); i++)
            params.push_back(words[i + 3]);
        block->setParams(params);
        std::getline(ifstream, buffer);
    }
    return blockMap;
}

void parseActions(std::map<std::string, WorkflowBlock *> &blockMap, std::ifstream &ifstream) {
    std::string buffer;
    std::getline(ifstream, buffer);
    std::vector<std::string> commands = split(buffer);
    std::list<std::string> text;
    ReturnType lastBlockReturnType = ReturnType::NONE;
    for (int i = 0; i < commands.size(); i++) {
        if (i % 2 == 1) {
            if (commands[i] != "->")
                throw WrongInstructionsFormatException();
            continue;
        }
        std::map<std::string, WorkflowBlock *>::iterator blockMapIterator;
        blockMapIterator = blockMap.find(commands[i]);
        if (blockMapIterator == blockMap.end())
            throw WrongInstructionsFormatException();
        WorkflowBlock *block = blockMapIterator->second;
        block->act(lastBlockReturnType, &text, block->getParams());
        lastBlockReturnType = block->getReturnType();
    }
}

void InstructionsParser::parseInstructions(const std::string &filename) {
    try {
        addAllBlocksToFactory();
        std::ifstream ifstream(filename);
        std::map<std::string, WorkflowBlock *> blockMap = parseDescription(ifstream);
        parseActions(blockMap, ifstream);
    }
    catch (WrongInstructionsFormatException& e)
    {
        std::cout << "Wrong instructions format" << std::endl;
    }
    catch (WrongBlockInputException& e)
    {
        std::cout << "Wrong input in block " << e.getId() << std::endl;
    }
}

//
// Created by Никита on 02.11.2017.
//

#ifndef WORKFLOW_INSTRUCTIONSPARSER_H
#define WORKFLOW_INSTRUCTIONSPARSER_H

#include <string>
#include <vector>

std::vector<std::string> split(const std::string& s);
class InstructionsParser {
public:
    void parseInstructions(const std::string &filename);
};


#endif //WORKFLOW_INSTRUCTIONSPARSER_H

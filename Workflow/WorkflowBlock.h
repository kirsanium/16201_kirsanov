#ifndef WORKFLOW_WORKFLOWBLOCK_H
#define WORKFLOW_WORKFLOWBLOCK_H

#include <string>
#include <list>

enum ReturnType {TEXT, NONE};

class WorkflowBlock {
    std::vector<std::string> params;
    std::string id;
public:
    virtual void act(ReturnType lastBlockReturnType, std::list<std::string> *text, std::vector<std::string> params) = 0;
    virtual ReturnType getReturnType() = 0;
    virtual ~WorkflowBlock(){};
    void set_id(const std::string &id) { this->id = id; };
    const std::string &get_id() const { return id; } ;
    void setParams(std::vector<std::string> params) { this->params = params; };
    std::vector<std::string> &getParams() { return this->params; };
    virtual int getParamAmount() = 0;
};

#endif //WORKFLOW_WORKFLOWBLOCK_H

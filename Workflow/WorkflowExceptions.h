#ifndef WORKFLOW_NOINPUTEXCEPTION_H
#define WORKFLOW_NOINPUTEXCEPTION_H


#include <exception>

class WorkflowExceptions: public std::exception {
public:
    WorkflowExceptions(){};
};

class WrongInstructionsFormatException: public std::exception {
public:
    WrongInstructionsFormatException(){};
};

class WrongBlockInputException: public std::exception {
    std::string blockId;
public:
    WrongBlockInputException(const std::string &id)
    {
        this->blockId = id;
    };
    std::string getId()
    {
        return this->blockId;
    };
};

class WrongInputException: std::exception {
public:
    WrongInputException(){};
};

#endif //WORKFLOW_NOINPUTEXCEPTION_H

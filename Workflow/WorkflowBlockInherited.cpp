#include <fstream>
#include <cstdarg>
#include <iostream>
#include <algorithm>
#include "WorkflowBlockInherited.h"
#include "WorkflowExceptions.h"

void ReadFileBlock::act(ReturnType lastBlockReturnType, std::list<std::string> *text, std::vector<std::string> params) {
    std::string filename = params[0];
    text->clear();
    std::ifstream ifstream(filename);
    std::string buffer;
    while (std::getline(ifstream, buffer)) {
        text->push_back(buffer);
    }
    ifstream.close();
}

void WriteFileBlock::act(ReturnType lastBlockReturnType, std::list<std::string> *text, std::vector<std::string> params) {
    if (lastBlockReturnType == ReturnType::NONE) {
        throw WrongBlockInputException(this->get_id());
    }
    std::string filename = params[0];
    std::ofstream ofstream(filename);
    while (!text->empty()) {
        long long int length = text->front().length();
        const char *strText = text->front().c_str();
        ofstream.write(strText, length);
        ofstream.write("\n", 1);
        text->pop_front();
    }
    ofstream.close();
}

void GrepBlock::act(ReturnType lastBlockReturnType, std::list<std::string> *text, std::vector<std::string> params) {
    if (lastBlockReturnType == ReturnType::NONE) {
        throw WrongBlockInputException(this->get_id());
    }
    std::string word = params[0];
    std::list<std::string>::iterator it = text->begin();
    std::list<std::string>::iterator it2;
    while (it != text->end()) {
        std::string line = *it;
        it2 = it;
        it++;
        if (line.find(word) == -1)
            text->erase(it2);
    }
}

void SortBlock::act(ReturnType lastBlockReturnType, std::list<std::string> *text, std::vector<std::string> params) {
    if (lastBlockReturnType == ReturnType::NONE)
        throw WrongBlockInputException(this->get_id());
    text->sort();
}

void ReplaceBlock::act(ReturnType lastBlockReturnType, std::list<std::string> *text, std::vector<std::string> params) {
    if (lastBlockReturnType == ReturnType::NONE) {
        throw WrongBlockInputException(this->get_id());
    }
    std::string word1 = params[0];
    std::string word2 = params[1];
    std::list<std::string>::iterator it = text->begin();
    while (it != text->end()) {
        std::string *line = &*it;
        size_t pos = line->find(word1);
        while (pos != -1) {
            size_t length = word1.length();
            line->replace(pos, length, word2);
            pos = line->find(word1);
        }
        it++;
    }
}
void addAllBlocksToFactory() {
    WorkflowBlockFactory::Instance().add<ReadFileBlock>("readfile");
    WorkflowBlockFactory::Instance().add<WriteFileBlock>("writefile");
    WorkflowBlockFactory::Instance().add<GrepBlock>("grep");
    WorkflowBlockFactory::Instance().add<SortBlock>("sort");
    WorkflowBlockFactory::Instance().add<ReplaceBlock>("replace");
}
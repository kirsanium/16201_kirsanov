#include <string>
#include <iostream>
#include <fstream>
#include <map>
#include <list>
#include "sort_strings.h"

using namespace std;

int main(int argc, char** argv) {
    if (argc != 3)
        std::cerr << "Use format: file_sort.exe input.txt output.txt" << std::endl;

    std::ifstream in(argv[1]);
    if (!in)
        std::cerr << "Unable to open input file" << std::endl;
    std::ofstream out(argv[2]);
    if (!out)
        std::cerr << "Unable to open output file" << std::endl;

    string buff;
    bool wordFound = false;
    list<pair<string, int>> myList;
    list<pair<string, int>>::iterator it;
    while (in >> buff) {
        for (it = myList.begin(); it != myList.end(); it++) {
            if (buff == it->first) {
                wordFound = true;
                break;
            }
        }
        if (!wordFound)
            myList.insert(it, pair<string, int>(buff, 1));
        else {
            it->second = it->second + 1;
        }
        wordFound = false;
    }
    myList.sort(sort_strings::sort_words);
    for (it = myList.begin(); it != myList.end(); it++) {
        out << it->first << " " << it->second << endl;
    }
    return 0;
}
#include <string>

namespace sort_strings {
    bool sort_words(const std::pair<std::string, int> &a, const std::pair<std::string, int> &b) {
        if (a.second == b.second) {
            return a.first.compare(b.first) < 0;
        } else return a.second > b.second;
    }
}
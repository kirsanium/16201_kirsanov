package spaceInvaders;

import javafx.application.Application;
import javafx.stage.Stage;
import spaceInvaders.view.GameView;

public class Main extends Application {

    private final double CANVAS_WIDTH  = 600;
    private final double CANVAS_HEIGHT = 600;

    @Override
    public void start(Stage primaryStage) throws Exception{

        GameView gameView = new GameView(CANVAS_WIDTH, CANVAS_HEIGHT);
        GameController gameController = new GameController(gameView);
        gameController.start(primaryStage);
    }

    public static void main(String[] args) {
        launch(args);
    }
}
package spaceInvaders.view;

import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class GameView {

    private final double CANVAS_WIDTH;
    private final double CANVAS_HEIGHT;

    private Stage stage;
    private Group root;
    public Scene scene;
    public GraphicsContext gc;

    public GameView(double canvasWidth, double canvasHeight){
        CANVAS_WIDTH  = canvasWidth;
        CANVAS_HEIGHT = canvasHeight;
    }

    public void initView(Stage stage) {
        this.stage = stage;
        stage.setTitle("Space Invaders");
        root = new Group();
        scene = new Scene(root);
        stage.setScene(scene);

        Canvas canvas = new Canvas(CANVAS_WIDTH, CANVAS_HEIGHT);
        root.getChildren().add(canvas);

        gc = canvas.getGraphicsContext2D();

        Font theFont = Font.font( "Helvetica", FontWeight.BOLD, 24 );
        gc.setFont( theFont );
        gc.setFill( Color.GREEN );
        gc.setStroke( Color.BLACK );
        gc.setLineWidth(1);
    }

    public double getCanvasWidth() {
        return CANVAS_WIDTH;
    }

    public double getCanvasHeight() {
        return CANVAS_HEIGHT;
    }

    public void showStage() {
        this.stage.show();
    }
}

package spaceInvaders.util;

import javafx.scene.image.Image;
import spaceInvaders.GameController;
import spaceInvaders.model.*;

public final class GameObjectFactory {

    public static final int PLAYER_SHIP     = 0;
    public static final int SHOT            = 1;
    public static final int ENEMY_SHIP      = 2;
    public static final int ENEMY_SHIPS_ROW = 3;
    public static final int ENEMY_SHOT      = 4;
    private static GameObjectFactory instance = null;

    private GameObjectFactory() {}

    public static GameObjectFactory getInstance() {
        if (instance == null) {
            GameObjectFactory gameObjectFactory = new GameObjectFactory();
            instance = gameObjectFactory;
        }
        return instance;
    }

    public GameObject create(int classNum, GameController controller, Image image) {
        GameObject object;
        switch (classNum) {
            case PLAYER_SHIP: {
                object = new PlayerShip(controller, image);
                break;
            }
            case SHOT: {
                object = new Shot(controller, image);
                break;
            }
            case ENEMY_SHIP: {
                object = new EnemyShip(controller, image);
                break;
            }
            case ENEMY_SHIPS_ROW: {
                object = new EnemyShipsRow(controller, image);
                break;
            }
            case ENEMY_SHOT: {
                object = new EnemyShot(controller, image);
                break;
            }
            default: {
                object = null;
                System.err.println("WTF???");
                System.exit(127);
            }
        }
        controller.addObj(object);
        return object;
    }
}

package spaceInvaders;

import javafx.animation.AnimationTimer;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import spaceInvaders.model.*;
import spaceInvaders.util.GameObjectFactory;
import spaceInvaders.util.IntWrap;
import spaceInvaders.util.LongWrap;
import spaceInvaders.view.GameView;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class GameController {

    private GameView gameView;

    private List<GameObject> objList = new ArrayList<>();
    private List<GameObject> toKillList = new ArrayList<>();
    private List<EnemyShip> enemyShipsList = new ArrayList<>();
    private List<EnemyShot> enemyShotsList = new ArrayList<>();
    private List<String> input = new ArrayList<>();
    private PlayerShip playerShip;
    private double psSpeed = 0.0;

    private double canvasWidth, canvasHeight;
    private final String PLAYER_SHIP_IMG = "file:resources/playership.png";

    public GameObjectFactory objFactory = GameObjectFactory.getInstance();

    public GameController(GameView gameView) {
        this.gameView = gameView;
    }

    public void start(Stage stage) {

        gameView.initView(stage);

        canvasWidth  = gameView.getCanvasWidth();
        canvasHeight = gameView.getCanvasHeight();

        gameView.scene.setOnKeyPressed(
                keyEvent -> {
                    String code = keyEvent.getCode().toString();
                    if(!input.contains(code))
                        input.add(code);
                }
        );

        gameView.scene.setOnKeyReleased(
                keyEvent -> {
                    String code = keyEvent.getCode().toString();
                    input.remove(code);
                }
        );


        startGame();

        LongWrap lastNanoTime = new LongWrap( System.nanoTime() );
        LongWrap lastNanoShootTime = new LongWrap(0);

        Random rand = new Random();
        IntWrap randInt = new IntWrap(0);
        IntWrap counter = new IntWrap(0);

        new AnimationTimer() {
            public void handle(long currentNanoTime) {
                // calculate time since last update.
                double elapsedTime = (currentNanoTime - lastNanoTime.value) / 1000000000.0;
                lastNanoTime.value = currentNanoTime;

                playerShip.setVelocity(0,0);
                if (input.contains("LEFT"))
                    playerShip.addVelocity(-psSpeed,0);
                if (input.contains("RIGHT"))
                    playerShip.addVelocity(psSpeed,0);
                if (input.contains("UP"))
                    playerShip.addVelocity(0,-psSpeed);
                if (input.contains("DOWN"))
                    playerShip.addVelocity(0,psSpeed);
                if (input.contains("SPACE")) {
                    if ((currentNanoTime - lastNanoShootTime.value) / 1000000000.0 > playerShip.getShootDelay()) {
                        playerShip.shoot();
                        lastNanoShootTime.value = currentNanoTime;
                    }
                }
                if (input.contains("ESCAPE"))
                    exitGame();

                randInt.value = rand.nextInt(enemyShipsList.size() * 100) + 1;
                counter.value = 0;
                for (EnemyShip enemyShip: enemyShipsList) {
                    ++counter.value;
                    if (randInt.value == counter.value) {
                        enemyShip.shoot();
                        break;
                    }
                }

                updateObjList(objList, toKillList, elapsedTime);

                killDeadObjects(objList, toKillList);
                toKillList.clear();

                if (enemyShipsList.isEmpty())
                    endGame();

                for (EnemyShip enemyShip: enemyShipsList) {
                    if (playerShip.intersects(enemyShip)) {
                        endGame();
                        break;
                    }
                }

                for (EnemyShot enemyShot: enemyShotsList) {
                    if (playerShip.intersects(enemyShot)) {
                        endGame();
                        break;
                    }
                }

                GraphicsContext gc = gameView.gc;
                gc.clearRect(0,0,canvasWidth, canvasHeight);
                renderObjList(objList, gc);
            }
        }.start();

        gameView.showStage();
    }

    private void endGame() {
        objList.clear();;
        toKillList.clear();
        enemyShipsList.clear();
        enemyShotsList.clear();
        startGame();
    }

    private void exitGame() {
        System.exit(0);
    }

    public void killDeadObjects(List<? extends GameObject> objList, List<GameObject> toKillList) {
        for (GameObject gameObject : toKillList) {
            gameObject.onDeath();
            objList.remove(gameObject);
        }
    }

    public void updateObjList(List<GameObject> objList, List<GameObject> toKillList, double elapsedTime) {
        for (GameObject gameObject : objList) {
            gameObject.update(elapsedTime);
            if (gameObject.toKill) {
                toKillList.add(gameObject);
            }
        }
    }

    public void renderObjList(List<GameObject> objList, GraphicsContext gc) {
        for (GameObject gameObject : objList)
            gameObject.render(gc);
    }

    private void startGame() {

        playerShip = (PlayerShip)objFactory.create(GameObjectFactory.PLAYER_SHIP, this, new Image(PLAYER_SHIP_IMG));
        psSpeed = playerShip.getSpeed();
        playerShip.setPosition(canvasWidth/2, canvasHeight/2);

        Image enemyShipImage = new Image(PLAYER_SHIP_IMG);
        int shipsInARow = 5;
        int rowsNum = 3;
        double startXOffset = 0.;
        double startYOffset = 0.;
        double xoffset = startXOffset;
        double yoffset = startYOffset;
        for (int i = 0; i < rowsNum; i++) {
            EnemyShipsRow row = (EnemyShipsRow)objFactory.create(GameObjectFactory.ENEMY_SHIPS_ROW, this, enemyShipImage);
            row.initRow(shipsInARow);
            row.setPosition(xoffset, yoffset);
            row.setStartYOffset(yoffset);
            yoffset += enemyShipImage.getHeight();
        }
    }

    public void addObj(GameObject obj) {
        objList.add(obj);
    }

    public List<EnemyShip> getEnemyShipsList() {
        return enemyShipsList;
    }

    public GameView getGameView() {
        return this.gameView;
    }

    public double getCanvasWidth() {
        return canvasWidth;
    }

    public double getCanvasHeight() {
        return canvasHeight;
    }

    public PlayerShip getPlayerShip() {
        return playerShip;
    }

    public List<EnemyShot> getEnemyShotsList() {
        return enemyShotsList;
    }

//    private class GameAnimationTimer extends AnimationTimer {
//
//        private LongWrap lastNanoTime = new LongWrap( System.nanoTime() );
//        private LongWrap lastNanoShootTime = new LongWrap(0);
//        private Random rand = new Random();
//        private IntWrap randInt = new IntWrap(0);
//        private IntWrap counter = new IntWrap(0);
//
//        public GameAnimationTimer() {
//            super();
//        }
//
//        @Override
//        public void handle(long currentNanoTime) {
//            // calculate time since last update.
//            double elapsedTime = (currentNanoTime - lastNanoTime.value) / 1000000000.0;
//            lastNanoTime.value = currentNanoTime;
//
//            playerShip.setVelocity(0,0);
//            if (input.contains("LEFT"))
//                playerShip.addVelocity(-psSpeed,0);
//            if (input.contains("RIGHT"))
//                playerShip.addVelocity(psSpeed,0);
//            if (input.contains("UP"))
//                playerShip.addVelocity(0,-psSpeed);
//            if (input.contains("DOWN"))
//                playerShip.addVelocity(0,psSpeed);
//            if (input.contains("SPACE")) {
//                if ((currentNanoTime - lastNanoShootTime.value) / 1000000000.0 > playerShip.getShootDelay()) {
//                    playerShip.shoot();
//                    lastNanoShootTime.value = currentNanoTime;
//                }
//            }
//            if (input.contains("ESCAPE"))
//                exitGame();
//
//            randInt.value = rand.nextInt(enemyShipsList.size() * 100) + 1;
//            counter.value = 0;
//            for (EnemyShip enemyShip: enemyShipsList) {
//                ++counter.value;
//                if (randInt.value == counter.value) {
//                    enemyShip.shoot();
//                    break;
//                }
//            }
//
//            updateObjList(objList, toKillList, elapsedTime);
//
//            killDeadObjects(objList, toKillList);
//            toKillList.clear();
//
//            if (enemyShipsList.isEmpty())
//                endGame();
//
//            for (EnemyShip enemyShip: enemyShipsList) {
//                if (playerShip.intersects(enemyShip)) {
//                    endGame();
//                    break;
//                }
//            }
//
//            for (EnemyShot enemyShot: enemyShotsList) {
//                if (playerShip.intersects(enemyShot)) {
//                    endGame();
//                    break;
//                }
//            }
//
//            GraphicsContext gc = gameView.gc;
//            gc.clearRect(0,0,canvasWidth, canvasHeight);
//            renderObjList(objList, gc);
//        }
//    }

}
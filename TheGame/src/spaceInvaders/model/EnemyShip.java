package spaceInvaders.model;

import javafx.scene.image.Image;
import spaceInvaders.GameController;
import spaceInvaders.util.GameObjectFactory;

public class EnemyShip extends GameObject {

    private final String SHOT_IMG = "file:resources/shot.png";

    private EnemyShipsRow row;
    private double rowOffset = 0;

    public EnemyShip(GameController controller, Image image) {
        super(controller, image);
        this.speed = 100;
    }

    public void shoot() {
        Image image = new Image(SHOT_IMG);
        EnemyShot shot = (EnemyShot)this.getController().objFactory.create(GameObjectFactory.ENEMY_SHOT, this.getController(), image);
        shot.setPosition(this.getPositionX() + this.getWidth() / 2, this.getPositionY() + 30);
    }

    @Override
    public void onDeath() {
        this.row.getShipsList().remove(this);
        this.getController().getEnemyShipsList().remove(this);
    }

    public void setRow(EnemyShipsRow row) { this.row = row; }

    public void setRowOffset(double offset) {this.rowOffset = offset;}

    public double getRowOffset() {
        return rowOffset;
    }

    public EnemyShipsRow getRow() { return this.row; }


}

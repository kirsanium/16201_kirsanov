package spaceInvaders.model;

import javafx.scene.image.Image;
import spaceInvaders.GameController;
import spaceInvaders.util.GameObjectFactory;

public class PlayerShip extends GameObject {

    private final String SHOT_IMG = "file:resources/shot.png";
    private final double SHOOT_DELAY = 0.5;

    public PlayerShip(GameController controller, Image image) {
        super(controller, image);
        this.velocityX = 0;
        this.velocityY = 0;
        this.speed = 250;
    }

    @Override
    public void update(double time) {
        double newPositionX = positionX + velocityX * time;
        double newPositionY = positionY + velocityY * time;
        if     (newPositionX + this.width < this.getController().getCanvasWidth() &&
                newPositionX > 0)
            positionX = newPositionX;

        if     (newPositionY + this.height < this.getController().getCanvasHeight() &&
                newPositionY > 0)
            positionY = newPositionY;

    }


    public void shoot() {
        Image image = new Image(SHOT_IMG);
        Shot shot = (Shot)this.getController().objFactory.create(GameObjectFactory.SHOT, this.getController(), image);
        shot.setPosition(this.getPositionX() + this.getWidth() / 2, this.getPositionY() - 30);
    }

    public double getShootDelay() {
        return SHOOT_DELAY;
    }
}

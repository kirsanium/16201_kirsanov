package spaceInvaders.model;

import javafx.scene.image.Image;
import spaceInvaders.GameController;
import spaceInvaders.util.GameObjectFactory;

import java.util.ArrayList;
import java.util.List;

public class EnemyShipsRow extends GameObject {

    private List<EnemyShip> shipsList = new ArrayList<>();
    private Image shipsImage;
    private double distBetweenShips = 10.0;
    private double currentMotionRow = 0;
    private double startYOffset = 0;

    public EnemyShipsRow(GameController controller, Image image) {
        super(controller);
        this.shipsImage = image;
        this.speed = 50;
        this.velocityX = speed;
        this.velocityY = 0;
    }

    public List<EnemyShip> getShipsList() { return shipsList; }

    public void initRow(int shipsNum) {
        for (int i = 0; i < shipsNum; i++) {
            EnemyShip ship = (EnemyShip)GameObjectFactory.getInstance().create(GameObjectFactory.ENEMY_SHIP, this.getController(), shipsImage);
            shipsList.add(ship);
            ship.setVelocity(velocityX, velocityY);
            ship.setRow(this);
            ship.setRowOffset(i * (distBetweenShips + ship.getWidth()));
        }
        this.height = shipsImage.getHeight();
        this.width = shipsImage.getWidth() * shipsNum + (shipsNum - 1) * distBetweenShips;
        this.getController().getEnemyShipsList().addAll(shipsList);
    }


    @Override
    public void update(double time) {
        boolean toSetPosX = false;
        boolean toSetPosY = false;
        double newPositionX = positionX + velocityX * time;
        double newPositionY = positionY + velocityY * time;
        if (newPositionX + this.width < this.getController().getCanvasWidth() && newPositionX > 0)
            toSetPosX = true;
        else {
            setVelocity(0, speed);
            ++currentMotionRow;
        }

        if (newPositionY < currentMotionRow * (getController().getCanvasHeight() / RowsMotionPolicy.MOTION_ROWS) + startYOffset)
            toSetPosY = true;
        else {
            if (currentMotionRow % 2 == 0)
                setVelocity(speed, 0);
            else
                setVelocity(-speed, 0);
        }

        if      (toSetPosX && !toSetPosY)
            setPosition(newPositionX, this.positionY);
        else if (!toSetPosX && toSetPosY)
            setPosition(this.positionX, newPositionY);
        else if (toSetPosX && toSetPosY)
            setPosition(newPositionX, newPositionY);

        if (this.getShipsList().isEmpty())
            this.toKill = true;
    }

    @Override
    public void setVelocity(double x, double y)
    {
        this.velocityX = x;
        this.velocityY = y;
        for (EnemyShip ship: shipsList) {
            ship.setVelocity(velocityX, velocityY);
        }
    }

    @Override
    public void setVelocityX(double velocityX)
    {
        this.velocityX = velocityX;
        for (EnemyShip ship: shipsList) {
            ship.setVelocityX(velocityX);
        }
    }

    @Override
    public void setVelocityY(double velocityY)
    {
        this.velocityY = velocityY;
        for (EnemyShip ship: shipsList) {
            ship.setVelocityY(velocityY);
        }
    }

    @Override
    public void setPosition(double x,  double y)
    {
        for (EnemyShip enemyShip: this.getShipsList()) {
            enemyShip.setPosition(x + enemyShip.getRowOffset(), y);
        }
        this.positionX = x;
        this.positionY = y;
    }

    public void setStartYOffset(double startYOffset) {
        this.startYOffset = startYOffset;
    }
}

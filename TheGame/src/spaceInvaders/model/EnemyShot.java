package spaceInvaders.model;

import javafx.scene.image.Image;
import spaceInvaders.GameController;

public class EnemyShot extends GameObject{
    public EnemyShot(GameController controller, Image image) {
        super(controller, image);
        this.speed = 200;
        this.velocityX = 0;
        this.velocityY = this.speed;
        controller.getEnemyShotsList().add(this);
    }

    @Override
    public void update(double time) {
        positionX += velocityX * time;
        positionY += velocityY * time;

        if (positionY + this.height < 0) {
            this.toKill = true;
        }
    }
}

package spaceInvaders.model;

import javafx.scene.image.Image;
import spaceInvaders.GameController;

public class Shot extends GameObject {
    public Shot(GameController controller, Image image) {
        super(controller, image);
        this.speed = 200;
        this.velocityX = 0;
        this.velocityY = -this.speed;
    }

    @Override
    public void update(double time) {
        positionX += velocityX * time;
        positionY += velocityY * time;

        for (EnemyShip enemyShip : this.getController().getEnemyShipsList()) {
            if (this.intersects(enemyShip)) {
                this.toKill = true;
                enemyShip.toKill = true;
                return;
            }
        }

        if (positionY + this.height < 0) {
            this.toKill = true;
        }
    }
}
